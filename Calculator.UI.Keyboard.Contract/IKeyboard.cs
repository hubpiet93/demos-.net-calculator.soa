﻿using System;
using System.Windows.Controls;

namespace Calculator.UI.Keyboard.Contract
{
    public interface IKeyboard
    {
        event EventHandler<KeyboardEventArgs> KeyPressed;
        event EventHandler<EnableEventArgs> EnableChanged;

        bool IsEnabled { get; }
        Control Control { get; }
    }
}
