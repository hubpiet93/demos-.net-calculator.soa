﻿using Calculator.ALU.Contract;
using System.Collections.Generic;

namespace Calculator.ALU
{
    public class BasicALUService : IALU
    {
        private List<ICalculateableOperation> operations;

        public BasicALUService(IEnumerable<ICalculateableOperation> operations)
        {
            this.operations = new List<ICalculateableOperation>(operations);
        }

        IEnumerable<Operation> IALU.AllowedOperations
        {
            get { return operations.ConvertAll<Operation>(e => e.Definiton); }
        }

        double IALU.PerformOperation(Operation operation, double number1, double number2)
        {
            return operations.Find(e => e.Definiton.Equals(operation)).Calculate(number1, number2);
        }
    }
}
