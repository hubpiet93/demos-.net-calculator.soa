﻿using Calculator.ALU.Contract;
using Calculator.ALU;
using Common.Logging;

namespace Calculator.ALU.Basic
{
    abstract class CalculateableOperation : ICalculateableOperation
    {
        public Operation Definiton { get; private set; }

        private readonly ILog logger;

        protected CalculateableOperation(string name, string symbol, ILog logger)
        {
            Definiton = new Operation(name, symbol);

            this.logger = logger;
        }

        public double Calculate(double number1, double number2)
        {
            double result = calculate(number1, number2);
            logger.Info(string.Format("{0} {1} {2} = {3}", number1, Definiton.Symbol, number2, result));
            return result;
        }

        protected abstract double calculate(double number1, double number2);
    }
    
    class Add : CalculateableOperation
    {
        public Add(ILog logger) : base("Add", "+", logger) { }

        protected override double calculate(double number1, double number2)
        {
            return number1 + number2;
        }
    }

    class Sub : CalculateableOperation
    {
        public Sub(ILog logger) : base("Sub", "-", logger) { }
        
        protected override double calculate(double number1, double number2)
        {
            return number1 - number2;
        }
    }

    class Mul : CalculateableOperation
    {
        public Mul(ILog logger) : base("Mul", "×", logger) { }

        protected override double calculate(double number1, double number2)
        {
            return number1 * number2;
        }
    }

    class Div : CalculateableOperation
    {
        public Div(ILog logger) : base("Div", "÷", logger) { }

        protected override double calculate(double number1, double number2)
        {
            return number1 / number2;
        }
    }
}
