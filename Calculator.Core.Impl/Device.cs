﻿using Calculator.ALU.Contract;
using Calculator.Core.Contract;
using Calculator.Memory.Contract;
using Common.Logging;
using System.Collections.Generic;

namespace Calculator.Core
{
    public class Device : ICalculations, IMemoryOperations
    {
        private ICalculations calculations;
        private IMemoryOperations memoryOperations;

        private readonly ILog logger;

        public Device(IALU alu, IMemoryManager memoryManager, IRegisterOperator registerOperator, ILog logger)
        {
            var accumulator = memoryManager.GetOrCreateRegister("Accumulator");
            var lastOperation = memoryManager.GetOrCreateRegister("LastOperation");
            var memory = memoryManager.GetOrCreateRegister("InternalMemory");
            calculations = new Calculations(alu, registerOperator, accumulator, lastOperation);
            memoryOperations = new MemoryOperations(registerOperator, memory, accumulator);

            this.logger = logger;
        }

        public ICalculations GetCalculations() { return calculations; }

        public IMemoryOperations GetMemoryOperations() { return memoryOperations; }

        IEnumerable<Operation> ICalculations.AllowedOperations
        {
            get { return calculations.AllowedOperations; }
        }

        void ICalculations.PerformOperation(Operation operation, double number)
        {
            logger.Info(string.Format("Perform operation {0} with number {1}", (operation != null) ? operation.Name : "null", number));
            calculations.PerformOperation(operation, number);
        }

        double ICalculations.CurrentValue
        {
            get 
            {
                logger.Info(string.Format("Current value is {0}", calculations.CurrentValue));
                return calculations.CurrentValue; 
            }
        }

        void ICalculations.Reset()
        {
            logger.Info("Calculations reset");
            calculations.Reset();
        }

        void IMemoryOperations.MemoryClear()
        {
            logger.Info("Memory clear");
            memoryOperations.MemoryClear();
        }

        double IMemoryOperations.MemoryRetrive()
        {
            double result = memoryOperations.MemoryRetrive();
            logger.Info(string.Format("Memory retrive: {0}", result));
            return result;
        }

        void IMemoryOperations.MemoryAdd(double number)
        {
            logger.Info(string.Format("Memory add {0}", number));
            memoryOperations.MemoryAdd(number);
        }

        void IMemoryOperations.MemorySub(double number)
        {
            logger.Info(string.Format("Memory subtract {0}", number));
            memoryOperations.MemorySub(number);
        }

        void IMemoryOperations.Reset()
        {
            logger.Info("Memory reset");
            memoryOperations.Reset();
        }
    }
}
