﻿using Calculator.Core.Contract;
using Calculator.Memory.Contract;

namespace Calculator.Core
{
    class MemoryOperations : IMemoryOperations
    {
        private Register memory;
        private Register accumulator;
        private IRegisterOperator registerOperator;

        public MemoryOperations(IRegisterOperator registerOperator, Register memory, Register accumulator)
        {
            this.registerOperator = registerOperator;
            this.memory = memory;
            this.accumulator = accumulator;
        }

        public double MemoryRetrive() 
        { 
            return (double)registerOperator.Retrive(memory);
        }

        public void MemoryAdd(double number) 
        {
            registerOperator.Store(memory, MemoryRetrive() + number); 
        }

        public void MemorySub(double number)
        {
            registerOperator.Store(memory, MemoryRetrive() - number); 
        }

        public void MemoryClear() 
        { 
            registerOperator.Clear(memory);
            registerOperator.Store(memory, 0.0);
        }

        public void Reset()
        {
            registerOperator.Clear(memory);
            registerOperator.Store(memory, 0.0);
            registerOperator.Clear(accumulator);
            registerOperator.Store(accumulator, 0.0);
        }
    }
}
