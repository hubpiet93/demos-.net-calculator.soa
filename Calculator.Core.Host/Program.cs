﻿using Calculator.ALU.Contract;
using Calculator.Core.Contract;
using Calculator.Core;
using Calculator.Memory.Contract;
using Ninject;
using Ninject.Extensions.Wcf;
using System;
using System.ServiceModel;
using Common.Logging;

namespace Calculator.Core.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<Device>().ToSelf().InSingletonScope();
            kernel.Bind<ICalculations>().To<Device>();
            kernel.Bind<IMemoryOperations>().To<Device>();
            kernel.Bind<IALU>().ToMethod(ctx =>
                {
                    var factory = new ChannelFactory<IALU>("alu");
                    return factory.CreateChannel();
                });
            kernel.Bind<IMemoryManager>().ToMethod(ctx =>
                {
                    var factory = new ChannelFactory<IMemoryManager>("mem");
                    return factory.CreateChannel();
                });
            kernel.Bind<IRegisterOperator>().ToMethod(ctx =>
                {
                    var factory = new ChannelFactory<IRegisterOperator>("reg");
                    return factory.CreateChannel();
                });
            kernel.Bind<ILog>().ToMethod(x => LogManager.GetLogger(x.Request.ParentRequest.Service.FullName));

            using (var host = kernel.Get<NinjectServiceHost<Device>>())
            {
                host.Open();

                Console.WriteLine("The service is ready at {0}", host.BaseAddresses[0]);
                Console.WriteLine("Press <Enter> to stop the service.");
                Console.ReadLine();

                host.Close();
            }
        }
    }
}
