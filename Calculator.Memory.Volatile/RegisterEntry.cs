﻿using Calculator.Memory.Contract;
using Common.Logging;

namespace Calculator.Memory
{
    class RegisterEntry
    {
        public Register RegisterId { get; private set; }

        object value;

        private readonly ILog logger;
        
        public RegisterEntry(string name, ILog logger)
        {
            RegisterId = new Register(name);

            this.logger = logger;
        }

        public object Retrive()
        {
            logger.Info(string.Format("Register {0}: retrive value ({1})", RegisterId.Name, (value != null) ? value : "null"));
            return value;
        }

        public void Store(object value)
        {
            logger.Info(string.Format("Register {0}: store value {1}", RegisterId.Name, value));
            this.value = value;
        }

        public void Clear()
        {
            logger.Info(string.Format("Register {0}: clear", RegisterId.Name, value));
            value = null;
        }
    }
}
