﻿using System.ServiceModel;

namespace Calculator.Core.Contract
{
    [ServiceContract]
    public interface IMemoryOperations
	{
        [OperationContract]
		void MemoryClear();

        [OperationContract]
		double MemoryRetrive();

        [OperationContract]
		void MemoryAdd(double number);

        [OperationContract]
        void MemorySub(double number);
      
        [OperationContract]
		void Reset();
	}
}
