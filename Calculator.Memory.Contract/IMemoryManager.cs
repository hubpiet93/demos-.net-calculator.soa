﻿using System.ServiceModel;

namespace Calculator.Memory.Contract
{
    [ServiceContract]
    public interface IMemoryManager
    {
        [OperationContract]
        Register CreateRegister(string name);

        [OperationContract]
        Register GetRegister(string name);

        [OperationContract]
        Register GetOrCreateRegister(string name);
    }
}
