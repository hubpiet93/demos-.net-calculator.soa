﻿using System.Runtime.Serialization;

namespace Calculator.Memory.Contract
{
    [DataContract]
    public class Register 
    {
        [DataMember]
        public string Name { get; private set; }

        public Register(string name)
        {
            Name = name;
        }
    }
}
