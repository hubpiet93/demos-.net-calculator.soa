﻿using Calculator.UI.Display.Contract;
using Ninject.Modules;

namespace Calculator.Infrastructure.Modules
{
    class DisplayModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDisplay>().To<Calculator.UI.Display.Display>().InSingletonScope();
        }
    }
}
