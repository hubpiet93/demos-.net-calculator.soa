﻿using Calculator.Memory.Contract;
using Common.Logging;
using Ninject;
using Ninject.Extensions.Wcf;
using System;


namespace Calculator.Memory.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<MemoryManager>().ToSelf().InSingletonScope();
            kernel.Bind<ILog>().ToMethod(x => LogManager.GetLogger(x.Request.ParentRequest.Service.FullName));

            using (var host = kernel.Get<NinjectServiceHost<MemoryManager>>())
            {
                host.Open();

                Console.WriteLine("The service is ready at {0}", host.BaseAddresses[0]);
                Console.WriteLine("Press <Enter> to stop the service.");
                Console.ReadLine();

                host.Close();
            }
        }
    }
}
