﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Calculator.UI.Main
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Control displayControl;
        private Control keyboardControl;

        public MainWindow(Control displayControl, Control keyboardControl)
        {
            this.displayControl = displayControl;
            this.keyboardControl = keyboardControl;
            InitializeComponent();
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            Grid.SetRow(displayControl, 0);
            var rdef = new RowDefinition();
            rdef.Height = new GridLength(100);
            this.Panel.RowDefinitions.Add(rdef);
            this.Panel.Children.Add(displayControl);

            Grid.SetRow(keyboardControl, 1);
            rdef = new RowDefinition();
            rdef.Height = new GridLength(300);
            this.Panel.RowDefinitions.Add(rdef);
            this.Panel.Children.Add(keyboardControl);
        }
    }
}
