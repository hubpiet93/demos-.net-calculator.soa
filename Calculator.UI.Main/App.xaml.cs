﻿using Calculator.Core.Contract;
using Calculator.Infrastructure;
using Calculator.UI.Display.Contract;
using Calculator.UI.Keyboard.Contract;
using Ninject;
using System;
using System.Windows;

namespace Calculator.UI.Main
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Controler controler;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var container = Configuration.Container;
            var display = container.Get<IDisplay>();
            var keyboard = container.Get<IKeyboard>();
            var calculations = container.Get<ICalculations>();
            var memoryOperations = container.Get<IMemoryOperations>();
            controler = new Controler(display, keyboard, memoryOperations, calculations);
            var mainWindow = new MainWindow(display.Control, keyboard.Control);
            mainWindow.Show();
        }
    }
}
