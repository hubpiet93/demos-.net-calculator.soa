﻿using Calculator.ALU;
using Calculator.ALU.Contract;
using Common.Logging;
using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Extensions.Wcf;
using System;

namespace Calculator.ALU.Host
{
    class Program
    {
        public static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<IALU>().To<BasicALUService>();
            kernel.Bind(x => x
                .FromAssemblyContaining<BasicALUService>()
                .IncludingNonePublicTypes()
                .SelectAllClasses()
                .InheritedFrom<ICalculateableOperation>()
                .BindAllInterfaces()
                .Configure(b => b.InSingletonScope()));
            kernel.Bind<ILog>().ToMethod(x => LogManager.GetLogger(x.Request.ParentRequest.Service.FullName));

            using (var host = kernel.Get<NinjectServiceHost<BasicALUService>>())
            {
                host.Open();

                Console.WriteLine("The service is ready at {0}", host.BaseAddresses[0]);
                Console.WriteLine("Press <Enter> to stop the service.");
                Console.ReadLine();

                host.Close();
            }
        }
    }
}
