﻿using System;
using System.Windows.Controls;

namespace Calculator.UI.Keyboard.Contract
{
    /// <summary>
    /// Interaction logic for KeyboardControl.xaml
    /// </summary>
    partial class KeyboardControl : UserControl
    {
        public KeyboardControl()
        {
            InitializeComponent();
        }
    }
}
